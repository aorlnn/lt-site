---
title: "Blade and Claw"
creator: "KD & SP"
meta_title: "Blade and Claw"
description: "Showcase page for Blade and Claw."
title_image: "images/showcase/blade-and-claw/0.png"
gallery_images: "images/showcase/blade-and-claw"
link: "https://feuniverse.us/t/complete-blade-and-claw-full-length-23-chapters/17017"
categories: [ "LT", "Complete" ]
tags: ["23 Chapters", "Original Game", "Classic Experience", "Fortnite"]
order: 5
draft: false
---

Blade and Claw is a complete, full-length tactical rpg featuring tight level design, a light-hearted narrative, and the return of the pair up mechanic.

Key Features:

- A unique stat system rebalance, focusing on making every level up feel exciting
- Fully fleshed out base conversations, adding depth to the entire cast
- A difficult but fair experience, perfect for ironmanners
