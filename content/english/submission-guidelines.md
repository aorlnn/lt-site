---
title: "Submission Guidelines"
meta_title: "Submission Guidelines"
description: "Guidelines for submitting games to the Showcase page"
draft: false
---

The gallery found on [Showcase]( {{< ref "showcase" >}} ) is used to show off games made with Lex Talionis that are planned to be finished. If you don't plan to finish the game in the forseeable feature, please wait until your project is more mature before submitting. Once you have compiled all the necessary information, reach out to the site maintainers in the [Discord Server](https://discord.gg/dC6VWGh4sw).

1. Make sure your game has a title screen, and provide an image of the title screen and any other game screenshots you want to showcase
2. Provide a link to where people can download the game
3. Provide a development state (Complete or WIP)
4. Provide a list of tags, include a tag of the chapter count only if your game is complete
5. Provide a description for your game
6. Provide a list of key features for your game
7. Optionally provide some reviews of your game
8. Optionally provide anything else you want to include
