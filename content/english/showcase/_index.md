---
title: "Showcase"
meta_title: "Showcase"
description: "A Showcase of work by some creators using the Lex Talionis engine."
draft: false
---

This is a showcase of some projects made by the creators in the [Discord Server](https://discord.gg/dC6VWGh4sw) using the Lex Talionis engine. All games are included here with permission from the creators and are presented for informational purposes only.

Want your game included here? Check out the [Submission Guidelines]( {{< ref "submission-guidelines" >}} ) and reach out to the site maintainers in the [Discord Server](https://discord.gg/dC6VWGh4sw).
