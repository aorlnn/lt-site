---
# Banner
banner:
  title: "Lex Talionis"
  content: "A Free and Open-Source GBA Fire Emblem Inspired Tactics Game Engine"
  image: "/images/logo-no-bg.png"
  buttons:
    - label: "Windows Download"
      link: "https://gitlab.com/rainlash/lt-maker/-/releases/permalink/latest/downloads/lex_talionis_maker"
    - label: "Getting Started"
      link: "https://lt-maker.readthedocs.io/en/latest/source/getting_started/Getting-Started.html"
  hero: "images/background.webp" # refers to the images in static/

# Features
features:
  - title: "Fully Featured Editor"
    content: "The newest version of Lex Talionis, LT-Maker is complete with a full user interface to help you build your games."
    images: "/images/editor"
  - title: "Powerful and Flexible"
    content: "Lex Talionis has a powerful and flexible system for adding items, skills, abilities, events, etc. Not impressed? Roll your own engine changes using Python code."
    image: "/images/mechanics.gif"
  - title: "Active Development"
    content: "Lex Talionis is changing every day with new features proposed and added by the community."
    image: "/images/development.png"
    image_link: "https://gitlab.com/rainlash/lt-maker"
    button:
      enable: true
      label: "Contribute"
      link: "https://gitlab.com/rainlash/lt-maker"
  - title: "Join the Community"
    content: "Come to our Discord server to chat and see what's being created."
    image: "/images/community.png"
    image_link: "https://discord.gg/dC6VWGh4sw"
    button:
      enable: true
      label: "Join Us"
      link: "https://discord.gg/dC6VWGh4sw"
  - title: "Game Showcase"
    content: "Take a look at some games made with Lex Talionis!"
    image: "/images/showcase.gif"
    button:
      enable: true
      label: "Check it Out"
      link: "showcase"

---
