---
title: "FE8R"
creator: "FE7Dragon"
meta_title: "FE8R"
description: "Showcase page for FE8R."
title_image: "images/showcase/fe8r/0.png"
gallery_images: "images/showcase/fe8r"
link: "https://feuniverse.us/t/lex-talionis-fire-emblem-8-reimagine-resource-beta-now-out-15-chapters-both-routes-playable/22653"
categories: [ "LT", "WIP" ]
tags: ["20 Chapters", "Remake", "Enhanced Remake"]
order: 10
draft: false
---

Relive the story of Fire Emblem: The Sacred Stones with more modern gameplay!

Key Features:

- Fates system durability, but with new weapons cooldown system.
- About 150+ new weapons and 60+ new spells
- Weapon types have new side effects (ex. Lances reduce damage taken based on SKL)
- Weapon ranks go beyond S rank
- There are 3 tiers of promotions (including Lords)
- Bows have WTA over magic and 2-3 range
- Every promotion has 3 choices (Including Lords)
- Lord promotions are not tied down to story events
- Units automatically promote at level 21
- Units have unlimited supports up to B, but only 1 A support.
- Base convos similar to FE9/10
- Pair up and Attack and Defense stances.
