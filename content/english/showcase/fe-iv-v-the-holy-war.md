---
title: "FE IV-V: The Holy War"
creator: "Axelloid"
meta_title: "FE IV-V: The Holy War"
description: "Showcase page for FE IV-V: The Holy War."
title_image: "images/showcase/fe-iv-v-the-holy-war/0.png"
gallery_images: "images/showcase/fe-iv-v-the-holy-war"
link: "https://feuniverse.us/t/fire-emblem-iv-v-the-holy-war-fe4-fe5-remake/9001"
categories: [ "LT", "WIP" ]
tags: ["Remake"]
order: 3
draft: false
---

A remake of Fire Emblem 4 and Fire Emblem 5 in the Lex Talionis engine.
