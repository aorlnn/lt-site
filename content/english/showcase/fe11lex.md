---
title: "FE11Lex"
creator: "Kinetic"
meta_title: "FE11Lex"
description: "Showcase page for FE11Lex."
title_image: "images/showcase/fe11lex/0.png"
gallery_images: "images/showcase/fe11lex"
link: "https://feuniverse.us/t/fe11lex-shadow-dragon-in-lt-prologue-1-chapter-1-demo-released/24203"
categories: [ "LT", "WIP" ]
tags: ["Remake"]
order: 13
draft: false
---

A recreation of FE11 in LT, with some changes added in.

Key Features:

- DSFE portraits ripped straight from FE12/FE11
- Maps remade with GBA tilesets
- Backgrounds ripped from FE12
- Base conversations in preparations to give characters some, well, actual character
- No skills!
- No reclassing. Instead, each character has a unique promotion out of an expanded pool of promoted classes; that Archer could promote into a Sniper or General! Several characters even have different unpromoted classes.
- Weapon forging
- Other balance changes, like less Warp uses, redone class stats, and adjustments to some characters stats.
