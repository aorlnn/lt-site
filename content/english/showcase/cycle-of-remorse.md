---
title: "Cycle of Remorse"
creator: "PKLucky"
meta_title: "Cycle of Remorse"
description: "Showcase page for Cycle of Remorse."
title_image: "images/showcase/cycle-of-remorse/0.png"
gallery_images: "images/showcase/cycle-of-remorse"
link: "https://feuniverse.us/t/lex-talionis-fire-emblem-cycle-of-remorse-version-0-11-0/19867"
categories: [ "LT", "WIP" ]
tags: ["26 Chapters", "Original Game", "Story-rich", "Ironman-friendly"]
order: 14
draft: false
---
Krista, princess of Medeas, sets off to investigate mysterious happenings in the neighboring country of Foliaga. She discovers that sinister things lie underneath her home continent of Yondo. Although, Krista herself also has something she has to hide...

Key Features:

- Linear, three-tier promotion system
- Low skill count for units (One personal skill per playable unit and 1-2 class skills)
- Supports
- Drawn CGs
- Tactics Logic: a story-centric game mechanic in which you have to come to logical conclusions to continue the story and give temporary boosts to your party
