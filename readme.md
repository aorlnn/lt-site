<h1 align="center">This website is the main page for the <a href="https://gitlab.com/rainlash/lt-maker">lt-maker community</a> made with <a href="https://github.com/zeon-studio/hugoplate">Hugoplate</a></h1>

## Getting Up To Speed

For contributors, you will need to get familiar with the following in order to effectively modify this website.

### HTML/CSS

You will need to understand the basics of HTML and CSS and how they are used to construct a site.

### Tailwind CSS

You will need to understand how to use Tailwind CSS (it's very similar to other CSS frameworks like Bootstrap if you used those before). [Documentation](https://tailwindcss.com/docs/utility-first).

### Hugo Templating System

You will need to understand how Hugo creates and configures its templates. [Video Tutorial](https://www.giraffeacademy.com/static-site-generators/hugo/). [Documentation](https://gohugo.io/documentation/).

### Other Things

#### Javascript

It's unlikely that you will need to know Javascript, there are usually existing modules from places like [GetHugoThemes](https://gethugothemes.com) to get the job done.

#### PostCSS

Tailwind is a PostCSS plugin. Unless you want to add other PostCSS functionality you don't need to know how PostCSS works.

---

**The following sections are taken directly from the Hugoplate readme and details how to setup and develop the site locally.**

## Getting Started

### Prerequisites

To start using this template, you need to have some prerequisites installed on your machine.

- [Hugo Extended v0.115+](https://gohugo.io/installation/)
- [Node v18+](https://nodejs.org/en/download/)
- [Go v1.20+](https://go.dev/doc/install)

### Install Dependencies

Install all the dependencies using the following command.

```bash
npm install
```

### Development Command

Start the development server using the following command.

```bash
npm run dev
```

---

## Customization

This template has been designed with a lot of customization options in mind. You can customize almost anything you want, including:

### Site Config

You can change the site title, base URL, language, theme, plugins, and more from the `hugo.toml` file.

### Site Params

You can customize all the parameters from the `config/_default/params.toml` file. This includes the logo, favicon, search, SEO metadata, and more.

### Colors and Fonts

You can change the colors and fonts from the `data/theme.json` file. This includes the primary color, secondary color, font family, and font size.

### Social Links

You can change the social links from the `data/social.json` file. Add your social links here, and they will automatically be displayed on the site.

---

## Advanced Usage

We have added some custom scripts to make your life easier. You can use these scripts to help you with your development.

### Update Theme

If you want to update the theme, then you can use the following command. It will update the theme to the latest version.

```bash
npm run update-theme
```

> **Note:** This command will work after running `project-setup` script.

### Update Modules

We have added a lot of modules to this template. You can update all the modules using the following command.

```bash
npm run update-modules
```

### Remove Dark Mode

If you want to remove dark mode from your project, then you have to do it manually from everywhere. So we build a custom script to do it for you. you can use the following command to remove dark mode from your project.

```bash
npm run remove-darkmode
```

> **Note:** This command will work before running `project-setup` script. If you already run the `project-setup` command, then you have to run `npm run theme-setup` first, and then you can run this command. afterward, you can run `npm run project-setup` again.

---

### Build Command

To build your project locally, you can use the following command. It will purge all the unused CSS and minify all the files.

```bash
npm run build
```
