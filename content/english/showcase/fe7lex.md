---
title: "FE7Lex"
creator: "beccarte"
meta_title: "FE7Lex"
description: "Showcase page for FE7Lex."
title_image: "images/showcase/fe7lex/0.png"
gallery_images: "images/showcase/fe7lex"
link: "https://feuniverse.us/t/fe7lex-beta-release-27-chapters-so-far/22079"
categories: [ "LT", "WIP" ]
tags: ["Remake", "Enhanced Remake", "Classic Experience", "Customizable"]
order: 4
draft: false
---

FE7Lex is a reproduction and enhancement of Fire Emblem 7 in the Lex Talionis engine. The goal is to replicate the features and feel of a modern Fire Emblem title. Importantly, the gameplay changes can be toggled on or off such that it can play either like the original game (“Retro” ruleset), a recent FE title (“Modern” ruleset), or some combination of the two using a built-in rule customization menu. New features in the Modern ruleset include a skill system, personal spell lists for caster units, pair-up combat for adjacent support partners, and many more. Regardless of whether the player opts for Retro or Modern mode, there are many new additions to the game’s presentation such as voiced units, colorized map sprites, critical hit cut-in animations, and updated combat animations.

Key Features:

- Skill system
- Branched promotion options
- Unlimited weapon uses with rebalanced effects
- Pair-up combat for support partners
- Personal spell lists for caster units
- Accessory-style stats boosters (instead of consumables)
- Varied shop inventories
- Modern bows (increased range but reduced accuracy with range)
- Dialogue revisions for specific scenes to fix plot inconsistencies.
