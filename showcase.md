# Adding Games to Showcase

## Instructions to Add to Website

To add new games to showcase the following should be done:

### 1. Create a new showcase directory for the picture gallery

Create a directory under `assets/images/showcase/` and name it the name of the game all lowercase, striped of special characters, and with spaces replaced with "-".

Example: "FE IV-V: The Holy War" -> `fe-iv-v-the-holy-war/`

### 2. Fill the gallery with images

Fill the gallery with .png files you want to use in your gallery. Make sure image names are named `0.png`, `1.png`, ..., `n.png` if you want to force the ordering in the gallery. If you don't care about ordering, at least name the image of the title screen `0.png`.

### 3. Create a new showcase page for the game

Create a new markdown file under `content/showcase/` and call it the name of the game all lowercase, striped of special characters, and with spaces replaced with "-" (Same as the gallery directory).

Example: "FE IV-V: The Holy War" -> `fe-iv-v-the-holy-war.md`

### 4. Fill the front matter of the .md file

Front matter is the YAML config data Hugo uses to populate some parts of the page. For reference, look at another game page (anything except `_index.md`) and go from there.

Example:

```yaml
---
title: "A Royal Summons"
creator: "Unimportant Gaming Studios"
meta_title: "A Royal Summons"
description: "Showcase page for A Royal Summons."
title_image: "images/showcase/a-royal-summons/0.png"
gallery_images: "images/showcase/a-royal-summons"
link: "https://feuniverse.us/t/complete-lex-talionis-a-royal-summons-a-one-chapter-game/20847"
categories: [ "LT", "Complete" ]
tags: ["1 Chapter", "Original Game", "Multiple Endings", "Unlockables", "Weird"]
order: 8
draft: false
---
```

### 5. Fix up the "order" field in the front matter

Order specifies where the game goes in the total order of where the games are displayed on the showcase page. Pick an appropriate location relative to everything else (likely the end, so take the highest order and +1).

### 6. Fill the markdown file with content

Fill the markdown file body with some content describing the game, and some key features, and optionally some reviews.
