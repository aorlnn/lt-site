---
title: "Absolution"
creator: "ZessDynamite"
meta_title: "Absolution"
description: "Showcase page for Absolution."
title_image: "images/showcase/absolution/0.png"
gallery_images: "images/showcase/absolution"
link: "https://feuniverse.us/t/lex-talionis-absolution-22-32-chapters/7045"
categories: [ "Legacy", "WIP" ]
tags: ["Original Game", "Original Soundtrack", "Tellius-inspired", "Story-rich"]
order: 2
draft: false
---

Built with the Legacy Engine. Absolution is a large-scale Fire Emblem adventure that spans multiple continents and features several unique mechanics, including a Tellius style skill system and a Paper Mario style cooking system. The game has over 30 chapters planned, with most sporting highly unique and memorable objectives. Help Valentina Bolivar free her people from oppression, overcome her personal demons, right the wrongs of millennia past, and fight onwards toward absolution!

Key Features:

- Tellius style skil system
- Paper Mario style cooking system
- Weapon crafting
- Personal skills
- All-original soundtrack!
