---
title: "Fate of the Fallen"
creator: "Unimportant Gaming Studios"
meta_title: "Fate of the Fallen"
description: "Showcase page for Fate of the Fallen."
title_image: "images/showcase/fate-of-the-fallen/0.png"
gallery_images: "images/showcase/fate-of-the-fallen"
link: "https://feuniverse.us/t/wip-lex-talionis-fate-of-the-fallen-10-22-chapters/20920"
categories: [ "LT", "WIP" ]
tags: ["Original Game", "Classic Experience", "Plot Twists", "Unlockables"]
order: 9
draft: false
---

An epic tale that follows the journey of Risariya Magnus de Magnyria as she picks herself off the ground, gathers allies, and defeats a wicked warlord. Best experienced without spoilers. Definitely no big twists here. Features include Supports, Skills, A Base System, Secret Items, Modifiable Genders, Achievements, Gaidens, Missable Units, 21 Chapters, 13 Epilogues, New items, and a dog.
