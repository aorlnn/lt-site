---
title: "Legacy"
meta_title: "Legacy"
description: "Category page for Legacy."
draft: false
---

Games made with the legacy Lex Talionis engine.
