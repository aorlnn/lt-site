---
title: "The Forgotten Schism"
creator: "JACK"
meta_title: "The Forgotten Schism"
description: "Showcase page for The Forgotten Schism."
title_image: "images/showcase/the-forgotten-schism/0.png"
gallery_images: "images/showcase/the-forgotten-schism"
link: "https://feuniverse.us/t/lt-demo-5-chapters-the-forgotten-schism/20885"
categories: [ "LT", "WIP" ]
tags: ["Original Game"]
order: 12
draft: false
---

In the impoverished highlands of Sarenas, a new story begins. When Vernados, a young bandit, is cornered by an army of invading foreigners, he awakens an ancient power that has been slumbering for centuries.
