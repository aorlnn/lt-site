---
title: "About"
meta_title: "About"
description: "About Lex Talionis"
draft: false
---

### The Engine

Create your game with Lex Talionis!

Creating your own 2D strategy, tactics, or RPG game made easy. The Lex Talionis engine, or LT-Maker for short, is a simple, easy-to-use tool for bringing your story to life.

Actively developed by a small but devoted group of developers, the Lex Talionis engine was custom built to build strategy RPGs. Everything you need comes prepackaged in the versatile editor, no programming experience necessary. But, if you really want to push your creativity to the limit, LT-maker offers a flexible Python backend for customizing the engine to meet your needs. From close to vanilla games like The Lion Throne or Fate of the Fallen, to crazy twists on the genre like Embrace of the Fog or Server 72, the ever evolving LT-Maker makes it all possible.

### Main Contributors

#### rainlash

Part-time international man of mystery, full-time Python enthusiast, rainlash is the original developer behind the Lex Talionis engine.

#### Mag

The enigmatic, mysterious, and secretive genius who has answered the call to action some years ago to turn Lex Talionis into the impressive feat it has become. We have her to thank for functions like the Overworld, and choice menu functionality. She knows everything, but her knowledge can only be earned by those who strive to uncover it for themselves.

#### KD

Developer of Blade & Claw, but also the source of the amazing Free Roam capabilities of the engine. He's a cool dude who once earned a fifth grade participation trophy for taekwondo. Sure, he was 20 years old at the time, but it still counts.