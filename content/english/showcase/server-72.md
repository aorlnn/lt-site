---
title: "Server 72"
creator: "Sigmaraven"
meta_title: "Server 72"
description: "Showcase page for Server 72."
title_image: "images/showcase/server-72/0.png"
gallery_images: "images/showcase/server-72"
link: "https://feuniverse.us/t/lt-server-72-demo/19970"
categories: [ "LT", "Complete" ]
tags: ["52 Chapters", "Original Game", "Dumb"]
order: 6
draft: false
---

Server 72 is a 52 chapter game about gamers, the least relatable of subjects.

Key Features:

- Several small mysteries culminating in an Ace Attorney-style trial
- Removal of several core FE gameplay features, such as permadeath, growth rates, class changes, staves, supports, and lords
- A prologue that requires roughly 10 resets on average
- Purchasable stats
- Forgeable weapons
- Innuendos
- Catgirl (singular)
- Turnwheel (you'll need it)
- Conversations about clipping, the future of technology, and horses
- Frustration

Reviews:

- "Has masterful understanding of the legal process" - Sir Spensir
- "It’s not the easiest general recommendation for every FE fan because of its departures from classic FE" - 4bundy
- “It’s like Dark Souls” - KD
- "I truly cannot recommend it" - HeroVP
- "Basically just Va-11 Hall-A except there are fewer lesbians." - CeaLumina
- "Understands MMOs better than SAO, I guess” - Epicer
- "I beat the prologue I guess" - Goldblitzx
- "Its just like if .hack was full of ffxiv players" - Mibbles
