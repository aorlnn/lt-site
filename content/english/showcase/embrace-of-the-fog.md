---
title: "Embrace of the Fog"
creator: "Lord Tweed"
meta_title: "Embrace of the Fog"
description: "Showcase page for Embrace of the Fog."
title_image: "images/showcase/embrace-of-the-fog/0.png"
gallery_images: "images/showcase/embrace-of-the-fog"
link: "https://feuniverse.us/t/lt-complete-1-0-embrace-of-the-fog-community-roguelite/21272"
categories: [ "LT", "Complete" ]
tags: ["Original Game", "Roguelite", "Replayable", "Unlockables"]
order: 11
draft: false
---

Embrace of the Fog is a Roguelite-styled adventure. Summon allies at random, empower them at your base, then journey through 3 acts of epic battles, strange events, and powerful bosses! This project consists of characters, maps, and items all submitted by the community, leading to a huge variety and nearly endless possibility.

Key Features:

- A massive roster of 200+ characters, all with unique gameplay.
- Over 70 different maps.
- A free-roaming base camp, where the player can perform permanent upgrades, speak with allies, summon new units, and more.
- 3 acts to venture through, where death is not the end of the journey. Should you fall, you can use your earned currencies to power up and challenge the fog again.
- A game that encourages mixed-phase gameplay, with system mechanics to match.
- A story that is delivered in fragments, needing to be pieced together to understand the full picture.
- Additional gameplay features, including a full codex for ailments and mechanics, a unit gallery,  and extended accessibility features.
