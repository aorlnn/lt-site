---
title: "The Raymond Chronicles"
creator: "BBHood217"
meta_title: "The Raymond Chronicles"
description: "Showcase page for The Raymond Chronicles."
title_image: "images/showcase/the-raymond-chronicles/0.png"
gallery_images: "images/showcase/the-raymond-chronicles"
link: "https://feuniverse.us/t/lex-talionis-the-raymond-chronicles/6814"
categories: [ "LT", "Complete" ]
tags: ["12 Chapters", "Original Game", "Classic Experience"]
order: 1
draft: false
---

Join Heather, Julie, Joshua, and many of MK404’s original characters on a multi-party adventure full of exciting gameplay, little references, and a standard plot to save the world.

Key Features:

- The Lion Throne ROM hack
- Different story than The Lion Throne
