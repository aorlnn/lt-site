---
title: "The Lion Throne"
creator: "rainlash"
meta_title: "The Lion Throne"
description: "Showcase page for The Lion Throne."
title_image: "images/showcase/the-lion-throne/0.png"
gallery_images: "images/showcase/the-lion-throne"
link: "https://gitlab.com/rainlash/lion-throne"
categories: [ "LT", "Complete" ]
tags: ["11 Chapters", "Original Game", "Classic Experience"]
order: 0
draft: false
---

The original game developed using the Lex Talionis engine. Heavily gameplay focused with major changes from the base Fire Emblem experience.

Key Features:

- Innovative objectives
- Powerful new items
- Custom classes
- Fully functioning skill system with activated skills
- Tellius-style base menu
- Much more!
