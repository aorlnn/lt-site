---
title: "A Royal Summons"
creator: "Unimportant Gaming Studios"
meta_title: "A Royal Summons"
description: "Showcase page for A Royal Summons."
title_image: "images/showcase/a-royal-summons/0.png"
gallery_images: "images/showcase/a-royal-summons"
link: "https://feuniverse.us/t/complete-lex-talionis-a-royal-summons-a-one-chapter-game/20847"
categories: [ "LT", "Complete" ]
tags: ["1 Chapter", "Original Game", "Multiple Endings", "Unlockables", "Weird"]
order: 8
draft: false
---

A wildly different experience brought to you by the Lex Talionis Engine. A Royal Summons is a character centric "one" chapter game meant to be played through 5 to 6 times. The game changes as you reach different endings, and the only way to progress to the end of the full story is to find every one.

Key Features:

- Unit Fusion Mechanics
- Supports
- Grandmaster Tactical Mode
- Achievements
- Randomized accessories
- A lord who can steal
- Betrayal
