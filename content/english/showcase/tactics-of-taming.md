---
title: "Tactics of Taming"
creator: "Unimportant Gaming Studios"
meta_title: "Tactics of Taming"
description: "Showcase page for Tactics of Taming."
title_image: "images/showcase/tactics-of-taming/0.png"
gallery_images: "images/showcase/tactics-of-taming"
link: "https://feuniverse.us/t/complete-lex-talionis-tactics-of-taming-marth-entry/20485"
categories: [ "LT", "Complete" ]
tags: ["1 Chapter", "Original Game", "Multiple Endings", "Unlockables"]
order: 7
draft: false
---

A game that feeds that need to catch 'em all! Tactics of Taming is a long-form single chapter game where your true strength comes from your power to make friends out of enemies.

Key Features:

- Unit Recruitment
- Monster units
- Pair Up
- A Large Map
- Supports
- Modifiers
- Achievements
- Easter Eggs.
